const fs = require('fs')
const path = require('path')

let inputFile = process.argv[2]

let inputStream = path.join(__dirname, '../req', inputFile)

let distance = (arr1, arr2) => {
  return (Math.abs(arr2[0] - arr1[0]) +  Math.abs(arr2[1] - arr1[1]))
}

fs.readFile(inputStream, 'utf8', (err, contents) => {

  // console.log(contents)

  let lines = contents.split('\n')
  let requirements = lines[0].split(' ')

  let rows = parseInt(requirements[0])
  let columns = parseInt(requirements[1])
  let fleets = parseInt(requirements[2])
  let rides = parseInt(requirements[3])
  let bonus = parseInt(requirements[4])
  let steps = parseInt(requirements[5])

  console.log({rows, columns, fleets, rides, bonus, steps})

  let trips = []
  let currentRide = 0

  let fleetNo = 0
  for (i = 1; i < rides; i++) {
    let lineDetail = lines[i].split(' ')
    let depart = [parseInt(lineDetail[0]), parseInt(lineDetail[1])]
    let dest = [parseInt(lineDetail[2]), parseInt(lineDetail[3])]
    let start = parseInt(lineDetail[4])
    let stop = parseInt(lineDetail[5])
    let dist = distance(depart, dest)
    // console.log({depart, dest, start, stop, dist})
    // console.log({dist, start, stop})

    // trips.push(i - 1)
    
    if (fleetNo < fleets) {
      if (!trips[fleetNo]) {
        trips[fleetNo] = []
      }
      trips[fleetNo].push(i -1)
      fleetNo += 1
    } else {
      fleetNo = 0
      if (!trips[fleetNo]) {
        trips[fleetNo] = []
      }
      trips[fleetNo].push(i -1)
    }
  }

  let result = ''

  trips.forEach((trip, index) => {
    result += trip.length + ' ' + trip.join(' ') + '\n' 
  })

  result = result.slice(0, result.length -1)

  fs.appendFileSync(`${inputFile.replace('in', 'out')}`, `${result}`)
})